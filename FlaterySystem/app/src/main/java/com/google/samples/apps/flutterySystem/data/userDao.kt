
package com.example.flaterysystem.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface userDao {
    @Query("SELECT * FROM user")
    fun gerAll():LiveData<List<User>>
    @Insert(onConflict = 1)
    fun insertAlluser(user:User):Long
}