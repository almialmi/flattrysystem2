

package com.google.samples.apps.flutterySystem.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.samples.apps.flutterySystem.PostedMaterialFragmentDirections
import com.google.samples.apps.flutterySystem.R
import com.google.samples.apps.flutterySystem.data.MaterialAndPostedMaterials
import com.google.samples.apps.flutterySystem.databinding.ListItemPostMaterialBinding
import com.google.samples.apps.flutterySystem.viewmodels.MaterialAndPostedMaterialViewModel

class PostedMaterialAdapter :
    ListAdapter<MaterialAndPostedMaterials, PostedMaterialAdapter.ViewHolder>(PostMaterialDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.list_item_post_material, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let { plantings ->
            with(holder) {
                itemView.tag = plantings
                bind(createOnClickListener(plantings.material.materialId), plantings)
            }
        }
    }

    private fun createOnClickListener(plantId: String): View.OnClickListener {
        return View.OnClickListener {
            //Data Bindings
                val direction =
                        PostedMaterialFragmentDirections.actionPostedMaterialFragmentToMaterialDetailFragment(plantId)
                it.findNavController().navigate(direction)
        }
    }

    class ViewHolder(
        private val binding: ListItemPostMaterialBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(listener: View.OnClickListener, materials: MaterialAndPostedMaterials) {
            with(binding) {
                clickListener = listener
                viewModel = MaterialAndPostedMaterialViewModel(materials)
                executePendingBindings()
            }
        }
    }
}

private class PostMaterialDiffCallback : DiffUtil.ItemCallback<MaterialAndPostedMaterials>() {

    override fun areItemsTheSame(
            oldItem: MaterialAndPostedMaterials,
            newItem: MaterialAndPostedMaterials
    ): Boolean {
        return oldItem.material.materialId == newItem.material.materialId
    }

    override fun areContentsTheSame(
            oldItem: MaterialAndPostedMaterials,
            newItem: MaterialAndPostedMaterials
    ): Boolean {
        return oldItem.material == newItem.material
    }
}