
package com.google.samples.apps.flutterySystem.viewmodels

import androidx.lifecycle.*
import com.google.samples.apps.flutterySystem.MaterialListFragment
import com.google.samples.apps.flutterySystem.data.Material
import com.google.samples.apps.flutterySystem.data.MaterialRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * The ViewModel for [MaterialListFragment].
 */
class MaterialListViewModel internal constructor(val materialRepository: MaterialRepository) : ViewModel() {

    private val price = MutableLiveData<Int>().apply { value = NO_PRICE }

    val materials: LiveData<List<Material>> = materialRepository.getMaterials()


    fun insertMaterial(material:Material)=viewModelScope.launch (Dispatchers.IO){
        materialRepository.insertMaterial(material)

    }


    fun setPrice(num: Int) {
        price.value = num
    }

    fun deletePrice() {
        price.value = NO_PRICE
    }

    fun isFiltered() = price.value != NO_PRICE

    companion object {
        private const val NO_PRICE = -1
    }
}
