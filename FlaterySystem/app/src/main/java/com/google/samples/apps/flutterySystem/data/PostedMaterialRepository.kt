package com.google.samples.apps.flutterySystem.data

import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

class PostedMaterialRepository private constructor(
    private val postedMaterialDao: PostedMaterialDao
) {

    suspend fun createPostedMaterials(materialId: String) {
        withContext(IO) {
            val postMaterial = PostedMaterial(materialId)
            postedMaterialDao.insertPostedMaterials(postMaterial)
        }
    }

    suspend fun removePostedMaterials(postedMaterial: PostedMaterial) {
        withContext(IO) {
            postedMaterialDao.deletePostedMaterials(postedMaterial)
        }
    }

    fun getPostedMaterialForMaterial(materialId: String) =
            postedMaterialDao.getPostedMaterialForMaterial(materialId)

    fun getPostedMaterials() = postedMaterialDao.getPostedMaterials()

    fun getMaterialAndPostedMaterials() = postedMaterialDao.getMaterialAndPostedMaterials()

    companion object {

        // For Singleton instantiation
        @Volatile private var instance: PostedMaterialRepository? = null

        fun getInstance(postedMaterialDao: PostedMaterialDao) =
                instance ?: synchronized(this) {
                    instance ?: PostedMaterialRepository(postedMaterialDao).also { instance = it }
                }
    }
}