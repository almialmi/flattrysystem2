
package com.example.flaterysystem.data

import androidx.lifecycle.LiveData

class UserRepository(private val userDao:userDao) {
    fun getAll():LiveData<List<User>> = userDao.gerAll()
    fun insertUser(user:User){
        userDao.insertAlluser(user)
    }
}