
package com.google.samples.apps.flutterySystem.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

import com.google.samples.apps.flutterySystem.data.PostedMaterialRepository
import com.google.samples.apps.flutterySystem.data.Material
import com.google.samples.apps.flutterySystem.data.MaterialRepository

/**
 * Factory for creating a [MaterialDetailViewModel] with a constructor that takes a [MaterialRepository]
 * and an ID for the current [Material].
 */
class MaterialDetailViewModelFactory(
        private val materialRepository: MaterialRepository,
        private val postedMaterialRepository: PostedMaterialRepository,
        private val plantId: String
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MaterialDetailViewModel(materialRepository, postedMaterialRepository, plantId) as T
    }
}
