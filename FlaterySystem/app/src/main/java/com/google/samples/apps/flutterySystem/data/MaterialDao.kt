
package com.google.samples.apps.flutterySystem.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * The Data Access Object for the Material class.
 */
@Dao
interface MaterialDao {
    @Query("SELECT * FROM materials ORDER BY name")
    fun getMaterials(): LiveData<List<Material>>

    @Query("SELECT * FROM materials WHERE price = :price ORDER BY name")
    fun getMaterialsWithPrice(price: Int): LiveData<List<Material>>

    @Query("SELECT * FROM materials WHERE id = :materialId")
    fun getMaterial(materialId: String): LiveData<Material>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(materials: List<Material>)
    @Insert
    fun insertOne(material:Material)
}
