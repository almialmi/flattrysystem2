package com.google.samples.apps.flutterySystem.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.samples.apps.flutterySystem.MaterialListFragment
import com.google.samples.apps.flutterySystem.MaterialListFragmentDirections
import com.google.samples.apps.flutterySystem.data.Material
import com.google.samples.apps.flutterySystem.databinding.ListItemMaterialBinding

/**
 * Adapter for the [RecyclerView] in [MaterialListFragment].
 */
class MaterialAdapter : ListAdapter<Material, MaterialAdapter.ViewHolder>(MaterialDiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val material = getItem(position)
        holder.apply {
            bind(createOnClickListener(material.materialId), material)
            itemView.tag = material
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ListItemMaterialBinding.inflate(
                LayoutInflater.from(parent.context), parent, false))
    }

    private fun createOnClickListener(materialId: String): View.OnClickListener {
        return View.OnClickListener {
          //Directions MaterialListFragment is generated
            val direction =MaterialListFragmentDirections.actionMaterialListFragmentToMaterialDetailFragment(materialId)
            it.findNavController().navigate(direction)
        }
    }

    class ViewHolder(
            //ListItemMaterial need To be generated
        private val binding: ListItemMaterialBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(listener: View.OnClickListener, item: Material) {
            binding.apply {
                clickListener = listener
                material = item
                executePendingBindings()
            }
        }
    }
}

private class MaterialDiffCallback : DiffUtil.ItemCallback<Material>() {

    override fun areItemsTheSame(oldItem: Material, newItem: Material): Boolean {
        return oldItem.materialId == newItem.materialId
    }

    override fun areContentsTheSame(oldItem: Material, newItem: Material): Boolean {
        return oldItem == newItem
    }
}