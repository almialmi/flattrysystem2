/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.flutterySystem.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.samples.apps.flutterySystem.data.Material
import com.google.samples.apps.flutterySystem.data.WebService
import com.google.samples.apps.flutterySystem.data.webServiceRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class WebServiceViewModel:ViewModel() {
    private val webServiceRepo:webServiceRepo
    init{

        val webService=WebService.getInstance()
        webServiceRepo= webServiceRepo(webService)
    }
    private val _getResponse=MutableLiveData<Response<Material>>()
    val getResponse:LiveData<Response<Material>>
    get() =_getResponse

    private val _insertResponse=MutableLiveData<Response<Material>>()
    val insertResponse:LiveData<Response<Material>>
        get() =_insertResponse

    fun getMaterilas(materialId:String)=viewModelScope.launch {
        _getResponse.postValue(webServiceRepo.getMaterialById(materialId))
    }
    fun insertMaterial(material:Material)=viewModelScope.launch {
        _insertResponse.postValue(webServiceRepo.insertMaterial(material))
    }

}