/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.flutterySystem

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR.viewModel
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import com.google.samples.apps.flutterySystem.adapters.NotificationsAdapter
import com.google.samples.apps.flutterySystem.data.Notfication
import com.google.samples.apps.flutterySystem.utilities.InjectorUtils
import com.google.samples.apps.flutterySystem.viewmodels.NotficationViewModel
import kotlinx.android.synthetic.main.fragment_material_notfication.view.*

class MaterialNotficationFragment : Fragment() {
    private val viewModel: NotficationViewModel by viewModels {
        InjectorUtils.provideNotficationViewModelFactory(requireContext())
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_material_notfication, container, false)
        val binding = FragmentMaterialNotficationBinding.inflate(inflater, container, false)
        context ?: return binding.root

        val adapter = NotificationsAdapter()
      //need adapter to correct
        binding.fragmentMaterialNotfication.adapter = adapter
        subscribeUi(adapter)
        super.onCreateView(inflater, container, savedInstanceState)
        setHasOptionsMenu(true)
        return binding.root
    }
    private fun subscribeUi(adapter: NotificationsAdapter) {
        viewModel.notfication.observe(viewLifecycleOwner) {notfication->
            if(notfication!= null) adapter.addNotif(notfication)

        }
    }



}