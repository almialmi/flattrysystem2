
package com.google.samples.apps.flutterySystem.viewmodels

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.ViewModel
import com.google.samples.apps.flutterySystem.data.MaterialAndPostedMaterials
import java.text.SimpleDateFormat
import java.util.Locale

class MaterialAndPostedMaterialViewModel(materials: MaterialAndPostedMaterials) : ViewModel() {

    private val material = checkNotNull(materials.material)
    private val postedMaterial = materials.postedMaterials[0]
    private val dateFormat by lazy { SimpleDateFormat("MMM d, yyyy", Locale.US) }
    val purchaseDateString =
        ObservableField<String>(dateFormat.format(postedMaterial.lastPurchaseDate.time))
    val lastsInt = ObservableInt(material.lastsInt)
    val imageUrl = ObservableField<String>(material.imageUrl)
    val materialName = ObservableField<String>(material.name)
    val materialDateString = ObservableField<String>(dateFormat.format(postedMaterial.materialDate.time))
}