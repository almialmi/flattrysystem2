
package com.google.samples.apps.flutterySystem.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.google.samples.apps.flutterySystem.data.PostedMaterialRepository
import com.google.samples.apps.flutterySystem.data.MaterialAndPostedMaterials

class PostedMaterialViewModel internal constructor(
        postedMaterialRepository: PostedMaterialRepository
) : ViewModel() {

    val postedMaterials = postedMaterialRepository.getPostedMaterials()

    val materialAndPostedMaterials: LiveData<List<MaterialAndPostedMaterials>> =
            postedMaterialRepository.getMaterialAndPostedMaterials().map { materials ->
                materials.filter { it.postedMaterials.isNotEmpty() }
            }
}