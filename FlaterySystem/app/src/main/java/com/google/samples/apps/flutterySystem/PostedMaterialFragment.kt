/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.flutterySystem

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import com.google.samples.apps.flutterySystem.adapters.PostedMaterialAdapter
import com.google.samples.apps.flutterySystem.databinding.FragmentPostedMaterialBinding
import com.google.samples.apps.flutterySystem.utilities.InjectorUtils
import com.google.samples.apps.flutterySystem.viewmodels.PostedMaterialViewModel

class PostedMaterialFragment : Fragment() {

    private val viewModel: PostedMaterialViewModel by viewModels {
        InjectorUtils.providePostedMaterialListViewModelFactory(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //binding
        val binding = FragmentPostedMaterialBinding.inflate(inflater, container, false)
        val adapter = PostedMaterialAdapter()
        binding.postedMaterialList.adapter = adapter
        subscribeUi(adapter, binding)
        return binding.root
    }

    private fun subscribeUi(adapter: PostedMaterialAdapter, binding: FragmentPostedMaterialBinding) {
        viewModel.postedMaterials.observe(viewLifecycleOwner) { materials ->
           //to be generated
            binding.hasMaterials = !materials.isNullOrEmpty()
        }

        viewModel.materialAndPostedMaterials.observe(viewLifecycleOwner) { result ->
            if (!result.isNullOrEmpty())
                adapter.submitList(result)
        }
    }
}
