/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.flutterySystem.data

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class NotficationRepo(val notficationDao:NotficationDao) {
    fun getNotfications()=notficationDao.getNotfications()

    suspend fun createNotfication(notifyId:String) {
        withContext(Dispatchers.IO) {
            val notfication = Notfication(notifyId)
            notficationDao.insertAll(notfication)
        }
    }
    companion object {

        // For Singleton instantiation
        @Volatile private var instance: NotficationRepo? = null

        fun getInstance(notficationDao: NotficationDao) =
                instance ?: synchronized(this) {
                    instance ?: NotficationRepo(notficationDao).also { instance = it }
                }
    }
}

