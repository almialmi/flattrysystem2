/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.flutterySystem

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.flaterysystem.data.User
import com.google.android.material.snackbar.Snackbar
import com.google.samples.apps.flutterySystem.data.Material
import com.google.samples.apps.flutterySystem.viewmodels.UserViewModel
import com.google.samples.apps.flutterySystem.viewmodels.WebServiceViewModel
import kotlinx.android.synthetic.main.fragment_apply.view.*

class ApplyFragment : Fragment() {
    lateinit var viewModel: UserViewModel
    lateinit var materialName: EditText
    lateinit var name: EditText
    lateinit var phoneNumber: EditText
    lateinit var applyButton: Button
    lateinit var viewModelApi:WebServiceViewModel
   // private var user: List<User> = emptyList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_apply, container, false)
        viewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        viewModelApi=ViewModelProviders.of(this).get(WebServiceViewModel::class.java)
        // Inflate the layout for this fragment

        materialName = view.mater_name
        name = view.per_name
        phoneNumber = view.phone
        applyButton = view.apply_btn1
        applyButton.setOnClickListener {

            val user = readField()
            viewModel.inserUser(user)
            viewModelApi.insertResponse.observe(this, Observer{
                response->response.body()?.run{
                updateFields(user)
            }
            })
            clearField()
            Snackbar.make(view, "We will call you soon", Snackbar.LENGTH_LONG).show()

        }
        return view


    }

    fun clearField(){

        phoneNumber.setText("")
        materialName.setText("")
        name.setText("")

    }
    fun updateFields(user: User) {
        user.run{
            phoneNumber.setText(phoneNo.toString())

        }


    }
    fun readField()= User(materialName.text.toString(),"",name.text.toString(),phoneNumber.text.toString())

}
