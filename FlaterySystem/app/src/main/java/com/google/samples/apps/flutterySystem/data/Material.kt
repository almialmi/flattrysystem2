package com.google.samples.apps.flutterySystem.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Calendar
import java.util.Calendar.DAY_OF_YEAR

@Entity(tableName = "materials")
data class Material(
        @PrimaryKey @ColumnInfo(name = "id") val materialId: String,
        val name: String,
        val description: String,
        val price: Int,
        val lastsInt: Int = 7, // how often the material should be displayed
        val imageUrl: String = ""
) {

    fun isAvailable(since: Calendar, lastPurchaseDate: Calendar) =
        since > lastPurchaseDate.apply { add(DAY_OF_YEAR, lastsInt) }

    override fun toString() = name
}
