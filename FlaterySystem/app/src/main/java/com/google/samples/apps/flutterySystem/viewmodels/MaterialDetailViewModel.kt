
package com.google.samples.apps.flutterySystem.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.map
import com.google.samples.apps.flutterySystem.MaterialDetailFragment
import com.google.samples.apps.flutterySystem.data.PostedMaterialRepository
import com.google.samples.apps.flutterySystem.data.Material
import com.google.samples.apps.flutterySystem.data.MaterialRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

/**
 * The ViewModel used in [MaterialDetailFragment].
 */
class MaterialDetailViewModel(
        materialRepository: MaterialRepository,
        private val postedMaterialRepository: PostedMaterialRepository,
        private val materialId: String
) : ViewModel() {

    val isPosted: LiveData<Boolean>
    val material: LiveData<Material>

    @ExperimentalCoroutinesApi
    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }

    init {
        val postedMaterialForMaterial = postedMaterialRepository.getPostedMaterialForMaterial(materialId)
        isPosted = postedMaterialForMaterial.map { it != null }

        material = materialRepository.getMaterial(materialId)
    }

    fun addMaterialToPostedMaterial() {
        viewModelScope.launch {
            postedMaterialRepository.createPostedMaterials(materialId)
        }
    }
}
