
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.flaterysystem.data.User
import com.example.flaterysystem.data.userDao

@Database(entities =[User::class],version = 1,exportSchema = false)
abstract class DatabaseUser:RoomDatabase(){
    abstract fun userDao(): userDao
    companion object{
        @Volatile
        private var INSTANCE:DatabaseUser?=null
        fun getDatabase(context:Context):DatabaseUser{

            val tempInstance= INSTANCE
            if(tempInstance!=null){
                return tempInstance
            }
            synchronized(this){
                val instance= Room.databaseBuilder(context.applicationContext,DatabaseUser::class.java,"user database").build()
                INSTANCE=instance
                return instance
            }
        }
    }
}