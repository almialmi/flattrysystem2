
package com.google.samples.apps.flutterySystem.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.samples.apps.flutterySystem.data.PostedMaterialRepository


class PostedMaterialViewModelFactory(
    private val repository: PostedMaterialRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return PostedMaterialViewModel(repository) as T
    }
}