
package com.google.samples.apps.flutterySystem.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

import com.google.samples.apps.flutterySystem.data.MaterialRepository

/**
 * Factory for creating a [MaterialListViewModel] with a constructor that takes a [MaterialRepository].
 */
class MaterialListViewModelFactory(
    private val repository: MaterialRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) = MaterialListViewModel(repository) as T
}
