
package com.google.samples.apps.flutterySystem.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.Calendar


@Entity(
    tableName = "posted_material",
    foreignKeys = [ForeignKey(entity = Material::class, parentColumns = ["id"], childColumns = ["material_id"])],
    indices = [Index("material_id")]
)
data class PostedMaterial(
        @ColumnInfo(name = "material_id") val plantId: String,

        @ColumnInfo(name = "material_date") val materialDate: Calendar = Calendar.getInstance(),

        @ColumnInfo(name = "last_purchase_date")
    val lastPurchaseDate: Calendar = Calendar.getInstance()
) {
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        var postMaterialId: Long = 0
}