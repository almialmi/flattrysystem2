
package com.google.samples.apps.flutterySystem.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.flaterysystem.data.User
import com.example.flaterysystem.data.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserViewModel(application:Application):AndroidViewModel(application) {
    private val userRepo:UserRepository
    val allUser: LiveData<List<User>>
    init{
        val userDao=DatabaseUser.getDatabase(application).userDao()
        userRepo= UserRepository(userDao)
        allUser=userRepo.getAll()
    }
    fun inserUser(user:User)=viewModelScope.launch(Dispatchers.IO){
        userRepo.insertUser(user)
    }
}