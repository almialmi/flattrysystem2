package com.google.samples.apps.flutterySystem.data

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface WebService {


    @GET("materials/{materialId}")
    fun getMaterialById(@Path("materialId") materialId: String): Deferred<Response<Material>>

    @GET("materials")
    fun getMaterials(): Deferred<Response<List<Material>>>

    @POST("materials")
    fun insertMaterials(@Body materials: Material): Deferred<Response<Material>>

    @PUT("materials/{materialId}")
    fun updateMaterials(@Path("materialId") materialId: String, @Body materials: Material): Deferred<Response<Material>>

    @DELETE("materials/{materialId}")
    fun deleteMaterial(@Path("materialId") materialId: String): Deferred<Response<Void>>

    companion object {

        private val baseUrl = "https://www.google.com"
        fun getInstance(): WebService {
            val materialRepo:MaterialRepository
            val retrofit: Retrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(MoshiConverterFactory.create())
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .build()

            val webService:WebService= retrofit.create(WebService::class.java)



            return webService


        }

    }
}