/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.flutterySystem.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.samples.apps.flutterySystem.R
import com.google.samples.apps.flutterySystem.data.Notfication
import com.google.samples.apps.flutterySystem.databinding.ListItemMaterialBinding
import com.google.samples.apps.flutterySystem.databinding.NotficationItemBinding

class NotificationsAdapter : RecyclerView.Adapter<NotificationsAdapter.NotificationHolder>() {
    private val notifs = mutableListOf<Notfication>()

    fun addNotif(notification: Notfication) {
        notifs.add(notification)
        notifyItemInserted(notifs.size)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationHolder {
        return NotificationHolder(NotficationItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int = notifs.size

    override fun onBindViewHolder(holder: NotificationHolder, position: Int) {
        holder.bind(notifs[position])
    }

    class NotificationHolder(private val binding: NotficationItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(notification: Notfication) {
            binding.notify = notification
        }

    }
}


