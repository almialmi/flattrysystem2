
package com.google.samples.apps.flutterySystem.utilities

import android.content.Context
import com.google.samples.apps.flutterySystem.data.AppDatabase
import com.google.samples.apps.flutterySystem.data.PostedMaterialRepository
import com.google.samples.apps.flutterySystem.data.MaterialRepository
import com.google.samples.apps.flutterySystem.data.NotficationRepo
import com.google.samples.apps.flutterySystem.viewmodels.PostedMaterialViewModelFactory
import com.google.samples.apps.flutterySystem.viewmodels.MaterialDetailViewModelFactory
import com.google.samples.apps.flutterySystem.viewmodels.MaterialListViewModelFactory
import com.google.samples.apps.flutterySystem.viewmodels.NotficationViewModelFactory


object InjectorUtils {

    private fun getMaterialRepository(context: Context): MaterialRepository {
        return MaterialRepository.getInstance(
                AppDatabase.getInstance(context.applicationContext).materialDao())
    }
    private fun getNotficationRepository(context: Context): NotficationRepo {
        return NotficationRepo.getInstance(
                AppDatabase.getInstance(context.applicationContext).notficatioDao())
    }

    private fun getPostedMaterialRepository(context: Context): PostedMaterialRepository {
        return PostedMaterialRepository.getInstance(
                AppDatabase.getInstance(context.applicationContext).postedMaterialDao())
    }

    fun providePostedMaterialListViewModelFactory(
        context: Context
    ): PostedMaterialViewModelFactory {
        val repository = getPostedMaterialRepository(context)
        return PostedMaterialViewModelFactory(repository)
    }

    fun provideMaterialListViewModelFactory(context: Context): MaterialListViewModelFactory {
        val repository = getMaterialRepository(context)
        return MaterialListViewModelFactory(repository)
    }
    fun provideNotficationViewModelFactory(context: Context): NotficationViewModelFactory {
        val repository = getNotficationRepository(context)
        return NotficationViewModelFactory(repository)
    }

    fun provideMaterialDetailViewModelFactory(
            context: Context,
            materialId: String
    ): MaterialDetailViewModelFactory {
        return MaterialDetailViewModelFactory(getMaterialRepository(context),
                getPostedMaterialRepository(context), materialId)
    }
}
