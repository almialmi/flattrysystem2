
package com.google.samples.apps.flutterySystem.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction

/**
 * The Data Access Object for the [PostedMaterial] class.
 */
@Dao
interface PostedMaterialDao {
    @Query("SELECT * FROM posted_material")
    fun getPostedMaterials(): LiveData<List<PostedMaterial>>

    @Query("SELECT * FROM posted_material WHERE id= :postedMaterialId")
    fun getPostedMaterial(postedMaterialId: Long): LiveData<PostedMaterial>

    @Query("SELECT * FROM posted_material WHERE material_id = :materialId")
    fun getPostedMaterialForMaterial(materialId: String): LiveData<PostedMaterial?>

    /**
     * This query will tell Room to query both the [Material] and [PostedMaterial] tables and handle
     * the object mapping.
     */
    @Transaction
    @Query("SELECT * FROM materials")
    fun getMaterialAndPostedMaterials(): LiveData<List<MaterialAndPostedMaterials>>

    @Insert
    fun insertPostedMaterials(postedMaterial: PostedMaterial): Long

    @Delete
    fun deletePostedMaterials(postedMaterial: PostedMaterial)
}