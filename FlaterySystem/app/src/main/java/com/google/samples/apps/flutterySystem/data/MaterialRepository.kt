
package com.google.samples.apps.flutterySystem.data
class MaterialRepository private constructor(private val materialDao: MaterialDao) {

    fun getMaterials() = materialDao.getMaterials()

    fun getMaterial(materialId: String) = materialDao.getMaterial(materialId)
    fun insertMaterial(material:Material)=materialDao.insertOne(material)
    fun getMaterialsWithPrice(price: Int) =
            materialDao.getMaterialsWithPrice(price)

    companion object {

        // For Singleton instantiation
        @Volatile private var instance: MaterialRepository? = null

        fun getInstance(materialDao: MaterialDao) =
                instance ?: synchronized(this) {
                    instance ?: MaterialRepository(materialDao).also { instance = it }
                }
    }
}
