/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.flutterySystem

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import com.google.samples.apps.flutterySystem.adapters.MaterialAdapter
import com.google.samples.apps.flutterySystem.databinding.FragmentMaterialListBinding
import com.google.samples.apps.flutterySystem.utilities.InjectorUtils
import com.google.samples.apps.flutterySystem.viewmodels.MaterialListViewModel

class MaterialListFragment : Fragment() {

    private val viewModel: MaterialListViewModel by viewModels {
        InjectorUtils.provideMaterialListViewModelFactory(requireContext())
    }
override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
}
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = FragmentMaterialListBinding.inflate(inflater, container, false)
        context ?: return binding.root

        val adapter = MaterialAdapter()
        binding.materialList.adapter = adapter
        subscribeUi(adapter)
        super.onCreateView(inflater, container, savedInstanceState)
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_material_list, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.filter_zone -> {
                activity?.let{
                    it.startActivity(Intent(it,AddMaterial::class.java))
                }

                //listener.onAddClicked()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun subscribeUi(adapter: MaterialAdapter) {
        viewModel.materials.observe(viewLifecycleOwner) { materials ->
            /**
             *  Material may return null, but the [observe] extension function assumes it will not be null.
             *  So there will be a warning（Condition `materials != null` is always `true`） here.
             *  I am not sure if the database return data type should be defined as nullable, Such as `LiveData<List<Material>?>` .
             */
            if (materials != null) adapter.submitList(materials)
        }
    }


    interface onAddClicked{
        fun onAddClicked()
        fun onNotification()
    }
    private lateinit var listener:onAddClicked
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is onAddClicked){
            listener=context
        }

    }
}