
package com.google.samples.apps.flutterySystem.data

import androidx.room.Embedded
import androidx.room.Relation

/**
 * This class captures the relationship between a [Material] and a user's [PostedMaterial], which is
 * used by Room to fetch the related entities.
 */
class MaterialAndPostedMaterials {

    @Embedded
    lateinit var material: Material

    @Relation(parentColumn = "id", entityColumn = "material_id")
    var postedMaterials: List<PostedMaterial> = arrayListOf()
}
