
package com.google.samples.apps.flutterySystem.utilities

const val DATABASE_NAME = "fluttery-db"
const val PLANT_DATA_FILENAME = "materials.json"