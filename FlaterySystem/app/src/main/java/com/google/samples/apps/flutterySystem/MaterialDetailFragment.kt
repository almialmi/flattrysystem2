/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.flutterySystem

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ShareCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import com.google.samples.apps.flutterySystem.databinding.FragmentMaterialDetailBinding
import com.google.samples.apps.flutterySystem.utilities.InjectorUtils
import com.google.samples.apps.flutterySystem.viewmodels.MaterialDetailViewModel
import com.google.samples.apps.flutterySystem.viewmodels.WebServiceViewModel

/**
 * A fragment representing a single Material detail screen.
 */
class MaterialDetailFragment : Fragment() {

//number one to be generated
    private val args: MaterialDetailFragmentArgs by navArgs()

    private lateinit var shareText: String
    lateinit var viewModelService:WebServiceViewModel
    private val materialDetailViewModel: MaterialDetailViewModel by viewModels {
        InjectorUtils.provideMaterialDetailViewModelFactory(requireActivity(), args.materialId)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentMaterialDetailBinding>(
                inflater, R.layout.fragment_material_detail, container, false).apply {
            viewModel = materialDetailViewModel
            lifecycleOwner = this@MaterialDetailFragment
            fab.setOnClickListener { view ->
                materialDetailViewModel.addMaterialToPostedMaterial()
                viewModelService.insertResponse.observe(lifecycleOwner as MaterialDetailFragment, Observer{
                    response->response.body()?.run{
                    Snackbar.make(view, R.string.update, Snackbar.LENGTH_LONG).show()
                }
                })
                Snackbar.make(view, R.string.added_material_to_posted_material, Snackbar.LENGTH_LONG).show()
            }
        }



        materialDetailViewModel.material.observe(this) { material ->
            shareText = if (material == null) {
                ""
            } else {
                getString(R.string.share_text_plant, material.name)
            }
        }

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_material_detail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    @Suppress("DEPRECATION")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_share -> {
                val shareIntent = ShareCompat.IntentBuilder.from(activity)
                    .setText(shareText)
                    .setType("text/plain")
                    .createChooserIntent()
                    .apply {
                        // https://android-developers.googleblog.com/2012/02/share-with-intents.html
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            // If we're on Lollipop, we can open the intent as a document
                            addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT or Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
                        } else {
                            // Else, we will use the old CLEAR_WHEN_TASK_RESET flag
                            addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
                        }
                    }
                startActivity(shareIntent)
                return true
            }
            R.id.action_apply ->{
                activity?.let {
                    it.startActivity(Intent(it, RequestActivity::class.java))
                }

                    return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
