/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.flutterySystem.data

import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.util.Calendar
import java.util.Calendar.DAY_OF_YEAR

class MaterialTest {


    private lateinit var material: Material

    @Before fun setUp() {
        material = Material("1", "Apple Computer", "An Apple Computer ", 1, 2, "")
    }

    @Test fun test_default_values() {
        val defaultMaterial = Material("2", "Apple Computer", "Description", 1)
        assertEquals(7, defaultMaterial.lastsInt)
        assertEquals("", defaultMaterial.imageUrl)
    }

    @Test fun test_shouldBeWatered() {
        Calendar.getInstance().let { now ->
            // Generate lastPurchaseDate from being as copy of now.
            val lastPurchaseDate = Calendar.getInstance()

            // Test for lastPurchaseDate is today.
            lastPurchaseDate.time = now.time
            assertFalse(material.isAvailable(now, lastPurchaseDate.apply { add(DAY_OF_YEAR, -0) }))

            // Test for lastPurchaseDate is yesterday.
            lastPurchaseDate.time = now.time
            assertFalse(material.isAvailable(now, lastPurchaseDate.apply { add(DAY_OF_YEAR, -1) }))

            // Test for lastPurchaseDate is the day before yesterday.
            lastPurchaseDate.time = now.time
            assertFalse(material.isAvailable(now, lastPurchaseDate.apply { add(DAY_OF_YEAR, -2) }))

            // Test for lastPurchaseDate is some days ago, three days ago, four days ago etc.
            lastPurchaseDate.time = now.time
            assertTrue(material.isAvailable(now, lastPurchaseDate.apply { add(DAY_OF_YEAR, -3) }))
        }
    }

    @Test fun test_toString() {
        assertEquals("Apple Computer", material.toString())
    }
}