
package com.google.samples.apps.flutterySystem.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.google.samples.apps.flutterySystem.utilities.getValue
import org.hamcrest.Matchers.equalTo
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MaterialDaoTest {
    private lateinit var database: AppDatabase
    private lateinit var materialDao: MaterialDao
    private val materialA = Material("1", "A", "", 1, 1, "")
    private val materialB = Material("2", "B", "", 1, 1, "")
    private val materialC = Material("3", "C", "", 2, 2, "")

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        materialDao = database.materialDao()

        // Insert materials in non-alphabetical order to test that results are sorted by name
        materialDao.insertAll(listOf(materialB, materialC, materialA))
    }

    @After fun closeDb() {
        database.close()
    }

    @Test fun testGetPlants() {
        val materialList = getValue(materialDao.getMaterials())
        assertThat(materialList.size, equalTo(3))

        // Ensure material list is sorted by name
        assertThat(materialList[0], equalTo(materialA))
        assertThat(materialList[1], equalTo(materialB))
        assertThat(materialList[2], equalTo(materialC))
    }

    @Test fun testGetPlantsWithGrowZoneNumber() {
        val materialList = getValue(materialDao.getMaterialsWithPrice(1))
        assertThat(materialList.size, equalTo(2))
        assertThat(getValue(materialDao.getMaterialsWithPrice(2)).size, equalTo(1))
        assertThat(getValue(materialDao.getMaterialsWithPrice(3)).size, equalTo(0))

        // Ensure material list is sorted by name
        assertThat(materialList[0], equalTo(materialA))
        assertThat(materialList[1], equalTo(materialB))
    }

    @Test fun testGetPlant() {
        assertThat(getValue(materialDao.getMaterial(materialA.materialId)), equalTo(materialA))
    }
}