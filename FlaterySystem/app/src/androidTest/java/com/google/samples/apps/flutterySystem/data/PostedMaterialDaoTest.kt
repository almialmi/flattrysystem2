
package com.google.samples.apps.flutterySystem.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.platform.app.InstrumentationRegistry
import com.google.samples.apps.flutterySystem.utilities.getValue
import com.google.samples.apps.flutterySystem.utilities.testCalendar
import com.google.samples.apps.flutterySystem.utilities.testGardenPlanting
import com.google.samples.apps.flutterySystem.utilities.testMaterial
import com.google.samples.apps.flutterySystem.utilities.testMaterials
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class PostedMaterialDaoTest {
    private lateinit var database: AppDatabase
    private lateinit var postedMaterialDao: PostedMaterialDao
    private var testPostedMaterialId: Long = 0

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        postedMaterialDao = database.postedMaterialDao()

        database.materialDao().insertAll(testMaterials)
        testPostedMaterialId = postedMaterialDao.insertPostedMaterials(testGardenPlanting)
    }

    @After fun closeDb() {
        database.close()
    }

    @Test fun testPostedMaterial() {
        val postedMaterial = PostedMaterial(
            testMaterials[1].materialId,
            testCalendar,
            testCalendar
        ).also { it.postMaterialId = 2 }
        postedMaterialDao.insertPostedMaterials(postedMaterial)
        assertThat(getValue(postedMaterialDao.getPostedMaterials()).size, equalTo(2))
    }

    @Test
    fun testGetPostedMaterial() {
        assertThat(
            getValue(postedMaterialDao.getPostedMaterial(testPostedMaterialId)),
            equalTo(testGardenPlanting)
        )
    }

    @Test fun testDeletePostedMaterial() {
        val gardenPlanting2 = PostedMaterial(
                testMaterials[1].materialId,
                testCalendar,
                testCalendar
        ).also { it.postMaterialId = 2 }
        postedMaterialDao.insertPostedMaterials(gardenPlanting2)
        assertThat(getValue(postedMaterialDao.getPostedMaterials()).size, equalTo(2))
        postedMaterialDao.deletePostedMaterials(gardenPlanting2)
        assertThat(getValue(postedMaterialDao.getPostedMaterials()).size, equalTo(1))
    }

    @Test fun testGetPostedMaterialForMaterial() {
        assertThat(getValue(postedMaterialDao.getPostedMaterialForMaterial(testMaterial.materialId)),
                equalTo(testGardenPlanting))
    }

    @Test fun testGetPostedMaterialForMaterials_notFound() {
        assertNull(getValue(postedMaterialDao.getPostedMaterialForMaterial(testMaterials[2].materialId)))
    }

    @Test fun testGetMaterialAndPostedMaterial() {
        val materialAndPostedMaterial = getValue(postedMaterialDao.getMaterialAndPostedMaterials())
        assertThat(materialAndPostedMaterial.size, equalTo(3))

        assertThat(materialAndPostedMaterial[0].material, equalTo(testMaterial))
        assertThat(materialAndPostedMaterial[0].postedMaterials.size, equalTo(1))
        assertThat(materialAndPostedMaterial[0].postedMaterials[0], equalTo(testGardenPlanting))

        // The other materials in the database have not been added and  thus have not displayed in postedMaterial
        assertThat(materialAndPostedMaterial[1].postedMaterials.size, equalTo(0))
        assertThat(materialAndPostedMaterial[2].postedMaterials.size, equalTo(0))
    }
}