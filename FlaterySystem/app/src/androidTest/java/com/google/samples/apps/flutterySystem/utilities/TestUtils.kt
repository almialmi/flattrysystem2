
package com.google.samples.apps.flutterySystem.utilities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.widget.Toolbar
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.espresso.intent.matcher.IntentMatchers.hasExtra
import com.google.samples.apps.flutterySystem.data.PostedMaterial
import com.google.samples.apps.flutterySystem.data.Material
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import java.util.Calendar

/**
 * [Material] objects used for tests.
 */
val testMaterials = arrayListOf(
    Material("1", "Apple Computer", "An Apple Computer ", 1),
    Material("2", "B", "Description B", 1),
    Material("3", "C", "Description C", 2)
)
val testMaterial = testMaterials[0]

/**
 * [Calendar] object used for tests.
 */
val testCalendar: Calendar = Calendar.getInstance().apply {
    set(Calendar.YEAR, 1998)
    set(Calendar.MONTH, Calendar.SEPTEMBER)
    set(Calendar.DAY_OF_MONTH, 4)
}

/**
 * [PostedMaterial] object used for tests.
 */
val testGardenPlanting = PostedMaterial(testMaterial.materialId, testCalendar, testCalendar)

/**
 * Returns the content description for the navigation button view in the toolbar.
 */
fun getToolbarNavigationContentDescription(activity: Activity, toolbarId: Int) =
    activity.findViewById<Toolbar>(toolbarId).navigationContentDescription as String


fun chooser(matcher: Matcher<Intent>): Matcher<Intent> = allOf(
    hasAction(Intent.ACTION_CHOOSER),
    hasExtra(`is`(Intent.EXTRA_INTENT), matcher)
)