
package com.google.samples.apps.flutterySystem.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.google.samples.apps.flutterySystem.data.AppDatabase
import com.google.samples.apps.flutterySystem.data.PostedMaterialRepository
import com.google.samples.apps.flutterySystem.data.MaterialRepository
import com.google.samples.apps.flutterySystem.utilities.getValue
import com.google.samples.apps.flutterySystem.utilities.testMaterial
import org.junit.After
import org.junit.Assert.assertFalse
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MaterialDetailViewModelTest {

    private lateinit var appDatabase: AppDatabase
    private lateinit var viewModel: MaterialDetailViewModel

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()

        val materialRepo = MaterialRepository.getInstance(appDatabase.materialDao())
        val postedMaterialRepo = PostedMaterialRepository.getInstance(
                appDatabase.postedMaterialDao())
        viewModel = MaterialDetailViewModel(materialRepo, postedMaterialRepo, testMaterial.materialId)
    }

    @After
    fun tearDown() {
        appDatabase.close()
    }

    @Test
    @Throws(InterruptedException::class)
    fun testDefaultValues() {
        assertFalse(getValue(viewModel.isPosted))
    }
}